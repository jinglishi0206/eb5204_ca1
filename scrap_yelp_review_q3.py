#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 24 00:07:01 2018

@author: JINGLISHI
"""


import re
import json
from feedparser import parse 
from requests import get
from bs4 import BeautifulSoup
from selenium import webdriver
import pandas as pd

# scrap 4 restaurant reviews from 4 category (Burgers, Italian,Japanese,Mexican)
c1_r1='https://www.yelp.com.sg/biz/pearls-deluxe-burgers-san-francisco-3'
c1_r2='https://www.yelp.com.sg/biz/jack-in-the-box-oakland'
c2_r1='https://www.yelp.com.sg/biz/sotto-mare-oysteria-and-seafood-san-francisco'
c2_r2='https://www.yelp.com.sg/biz/parmigiano-pizza-and-burger-south-san-francisco-3'
c3_r1='https://www.yelp.com.sg/biz/ryokos-san-francisco'
c3_r2='https://www.yelp.com.sg/biz/floating-sushi-boat-san-francisco'
c4_r1='https://www.yelp.com.sg/biz/el-farolito-san-francisco-2'
c4_r2='https://www.yelp.com.sg/biz/chipotle-mexican-grill-san-francisco-18'


def removeIndent(phrase):
    phrase=re.sub("\n",' ',phrase)
    phrase=re.sub("\r",' ',phrase)
    phrase=re.sub("\t",' ',phrase)
    return phrase
 
yelp_categories = ['Burgers','Italian','Japanese','Mexican']
 
yelp_urls = [
        [c1_r1,c1_r1+'?start=20'],
        [c1_r2,c1_r2+'?start=20'],
        [c2_r1,c2_r1+'?start=20'],
        [c2_r2,c2_r2+'?start=20'],
        [c3_r1,c3_r1+'?start=20'],
        [c3_r2,c3_r2+'?start=20'],
        [c4_r1,c4_r1+'?start=20'],
        [c4_r2,c4_r2+'?start=20']
        ]
df_q3 = pd.DataFrame(columns={'text','Sentiment'})
print(len(yelp_urls[0]))
business_ids=[]
print(len(yelp_urls))
for u in range(len(yelp_urls)):
    for v in range(len(yelp_urls[u])):
        url = yelp_urls[u][v]
        url_content=get(url)
        page = url_content.content.decode('utf-8','ignore')
        soup = BeautifulSoup(page, 'html.parser')
        b = soup.find_all('script')

        bid = ""
        for i in range(len(b)):
            if "business_id" in b[i].text:
                idx = b[i].text.index("business_id")
                idx2 = b[i].text.index("biz_has_menu")
                t1 = b[i].text[idx+13:idx2-3]
                idx = t1.index('"')
                idx2 = t1[idx+1:].index('"')
                bid = t1[idx+1:idx2+idx+1]
                break
        business_ids.append(bid)
        print('busines id: ', bid)

        list_data = soup.find_all("script", type="application/ld+json")
        for i in range(len(list_data)):
            if 'aggregateRating' in list_data[i].text:
                data = list_data[i].text.lstrip().rstrip()    
        data = removeIndent(data)     
        jsondata = json.loads(data)
        
        for j in range(len(jsondata['review'])):
            txt = jsondata['review'][j]['description']
            se = 'positive' if int(jsondata['review'][0]['reviewRating']['ratingValue']) > 3 else 'negative'
            df_q3 = df_q3.append({'text': txt,
                                  'Sentiment' : se
                                 },ignore_index=True)
    
df_q3.to_csv('yelp_scrap_review_q3.csv',index=False)
print('done')
