#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 20 23:02:50 2018

@author: JINGLISHI
"""

import re
import json
from feedparser import parse 
from requests import get
from bs4 import BeautifulSoup
from selenium import webdriver
import pandas as pd


def removeIndent(phrase):
    phrase=re.sub("\n",' ',phrase)
    phrase=re.sub("\r",' ',phrase)
    phrase=re.sub("\t",' ',phrase)
    return phrase

# for yelp restaurant, 20 reviews per page
r1 = "https://www.yelp.com.sg/biz/san-tung-san-francisco-2"
r2 = 'https://www.yelp.com.sg/biz/hunan-homes-restaurant-san-francisco?start=20'
r3 = 'https://www.yelp.com.sg/biz/mission-chinese-food-san-francisco-4'
r4 = 'https://www.yelp.com.sg/biz/sam-wo-restaurant-san-francisco-3'
r5 = 'https://www.yelp.com.sg/biz/kung-food-san-francisco'
r6 = 'https://www.yelp.com.sg/biz/dong-bei-mama-san-francisco'
r7 = 'https://www.yelp.com.sg/biz/heung-yuen-restaurant-san-francisco'
r8 = 'https://www.yelp.com.sg/biz/chong-qing-xiao-mian-san-francisco'
r9 = 'https://www.yelp.com.sg/biz/dumpling-kitchen-san-francisco'
r10 = 'https://www.yelp.com.sg/biz/yans-kitchen-san-francisco'

yelp_urls = [[r1,r1+'?start=20',r1+'?start=40'],
             [r2, r2+'?start=20',r2+'?start=40'],
             [r2, r2+'?start=20',r2+'?start=40'],
             [r2, r2+'?start=20',r2+'?start=40'],
             [r2, r2+'?start=20',r2+'?start=40'],
             [r2, r2+'?start=20',r2+'?start=40'],
             [r2, r2+'?start=20',r2+'?start=40'],
             [r2, r2+'?start=20',r2+'?start=40'],
             [r2, r2+'?start=20',r2+'?start=40'],
             [r2, r2+'?start=20',r2+'?start=40']]

df_q1 = pd.DataFrame(columns={'text','Sentiment'})

for u in range(len(yelp_urls)):
    for v in range(len(yelp_urls[u])):
        url = yelp_urls[u][v]
        url_content=get(url)
        page = url_content.content.decode('utf-8','ignore')
        soup = BeautifulSoup(page, 'html.parser')
        list_data = soup.find_all("script", type="application/ld+json")
        for i in range(len(list_data)):
            if 'aggregateRating' in list_data[i].text:
                data = list_data[i].text.lstrip().rstrip()    
        data = removeIndent(data)     
        jsondata = json.loads(data)
        
        for j in range(len(jsondata['review'])):
            txt = jsondata['review'][j]['description']
            se = 'positive' if int(jsondata['review'][0]['reviewRating']['ratingValue']) > 3 else 'negative'
            df_q1 = df_q1.append({'text': txt,
                                  'Sentiment' : se
                                 },ignore_index=True)


df_q1.to_csv('yelp_scrap_review_q1.csv',index=False)


