#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 27 14:43:08 2018

@author: JINGLISHI
"""
import pandas as pd
import seaborn as sns
sns.set()

non_neg_acc = [0.8,0.9,0.6,0.8]
neg_acc = [0.7,0.85,0.5,0.65]
idx = ['NB','SVM',
      'Non-Neg Test','Neg Test']
df_acc = pd.DataFrame({'Non-Neg':non_neg_acc,'Neg':neg_acc},index=idx)
ax = df_acc.plot.bar(rot=0)
 